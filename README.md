# Bus Reservation System

This is a retired college assignment I made in 2020, for my Software Design Lab (18CSL58, [GIT](http://git.edu)). 
It uses Apache, phpMyAdmin and MySQL (AMP).

### Try it out:

1. Clone this project, open Terminal in your ```http``` or ```www``` folder of your AMP installation and type:<br>
```git clone https://gitlab.com/thomascat/busreservationsystem```
<b>OR</b>
Download the zip and extract it to ```http``` or ```www``` folder of your AMP installation.

2. Go to ```localhost/busreservationsystem/welcome.html``` in your web browser.


<b>Tip</b>: To quickly set up the required database, open the ```dbcreator.php``` file in your web browser.

<hr>

[Open-source notices](NOTICE)

<b>License</b>: 

<a href="http://www.gnu.org/licenses/gpl-3.0.en.html" rel="nofollow"><img src="https://camo.githubusercontent.com/0e71b2b50532b8f93538000b46c70a78007d0117/68747470733a2f2f7777772e676e752e6f72672f67726170686963732f67706c76332d3132377835312e706e67" alt="GNU GPLv3 Image" data-canonical-src="https://www.gnu.org/graphics/gplv3-127x51.png" width="80"></a><br>[Copyright © 2017  Owais Shaikh](LICENSE)


